function dxdt = dyneq_pendulum(t, x, options);

global g;
global L; 
x = reshape(x, 2, size(x,1)/2);

dxdt = [x(2,:);...
        (-g/L).*sin((x(1,:)))];
    
dxdt  = reshape(dxdt, size(dxdt,1)*size(dxdt,2), 1);