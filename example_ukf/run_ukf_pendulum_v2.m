%--------------------------------------------------------------------
% Kalman Filtering
% for a Continuous-Time Mechanical System (Simple Pendulum)
%
% Bruno Teixeira, 17 Sept 2006, 29 Aug 2014 (last update)
%--------------------------------------------------------------------

close all; clear all; clear functions;
disp('Unscented Kalman Filtering'); disp('');
%--------------------------------------------------------------------
% Gererate the data:
% addpath('../kalman');
% Model:
global g;  g = 9.81;
global L;  L = 1;
global dt; dt = 0.1;
global deltah; deltah = 0.001;
mass = 1;
C = [0 1];
N = 30/dt;
x = [3/4*pi 1*pi/50]';
stdr = [0.25];
% global Q; 
Q = 1e-8.*eye(2); % To ensure convergence
options = odeset('MaxStep', dt, 'RelTol', 1e-12);

j_max = 25;

% Unscented parameters:
ffun      = 'eq_dyn_pendulum';            
hfun      = 'eq_obs_pendulum'; 

%--------------------------------------------------------------------
% Generate data:

r = stdr.*randn(size(C,1),N);
R = stdr^2.*eye(size(C,1));

for k = 2:N 
    [t, aux] = ode45('dyneq_pendulum', [0 dt], x(:,k-1), options);
    x(:,k) = aux(end,:)';
    y(:,k) = C*x(:,k) + r(:,k);
end

figure(1); plot(x'); title('x'); figure; plot(y'); title('y');


%--------------------------------------------------------------------
% Kalman Filtering:

% Initial parameters:
x0 = [0.5 0.5]';
x0 = x(:,1);
P0 = diag([0.05 0.05]);
xe = x0; Pe = P0; Pdiag(:,1) = diag(Pe);  inov1(:,1) = [0]'; cons1y(:,1) = 2.71; cons1x(:,1) = 4.61; P1yy(1) = R;

tol_cons = 1e-16;
perc = 0.95;

for k = 2:N
    % UKF:
    [xe(:,k), Pe, inov1(:,k), P1yy(k)] = aukf(xe(:,k-1)', Pe, dt*Q, R, y(:,k), ffun, hfun, dt);
    Pdiag = [Pdiag diag(Pe)];
    [cons1y(1,k), confy] = consistency_test(inov1(:,k), P1yy(k), perc, tol_cons);
    [cons1x(1,k), confx] = consistency_test(x(:,k)-xe(:,k), Pe, perc, tol_cons); 
end
% 
[rho1, delay, conf1] = whiteness_test(inov1, j_max, perc);

% Plots - Estimate errors:
t = [0:dt:dt*(N-1)];
figure(10); subplot(311);
plot(t, x(1,:)-xe(1,:), 'b');  hold on;
plot(t, -3.*sqrt(Pdiag(1,:)),'b:');  plot(t, 3.*sqrt(Pdiag(1,:)),'b:');
axis([0 t(end) -0.5 0.5]); 
xlabel('t (s)'); ylabel('e_{\theta} (rad)');
%
figure(10); subplot(312);
plot(t, x(2,:)-xe(2,:), 'b'); hold on; 
plot(t, -3.*sqrt(Pdiag(2,:)),'b:');  plot(t, 3.*sqrt(Pdiag(2,:)),'b:');
axis([0 t(end) -0.75 0.75]); 
xlabel('t (s)'); ylabel('e_{\omega} (rad/s)');

figure(11);
subplot(311);
plot(t, log10(abs(x(1,:)-xe(1,:))), 'b'); 
xlabel('t (s)'); ylabel('log_{10}(e_{\theta})');
subplot(312);
plot(t, log10(abs(x(2,:)-xe(2,:))), 'b'); 
xlabel('t (s)'); ylabel('log_{10}(e_{\omega})');


% Innovation:
figure(10); subplot(313);
plot(t, inov1, 'r'); hold on; 
plot(t, -3.*sqrt(P1yy),'r:');  plot(t, 3.*sqrt(P1yy),'r:');
xlabel('t (s)'); ylabel('\eta (rad/s)');
%
figure(11);
subplot(313);
plot(t, log10(abs(inov1)), 'r'); 
xlabel('t (s)'); ylabel('log_{10}(\eta)');

% Performance indices: Information:
meanp_trace1 = mean(Pdiag(1,1:end) + Pdiag(2,1:end))

% Performance indices: Accuracy:
RMSE_kf1 = sqrt(sum((x(1,1:end) - xe(1,1:end)).^2)/(length(x(1,1:end))))
RMSE_kf2 = sqrt(sum((x(2,1:end) - xe(2,1:end)).^2)/(length(x(2,1:end))))

% % Consistency check:
figure; %subplot(211);
plot(t, cons1y, 'b','LineWidth',2); hold on;
plot(t, confy(2)*ones(1,length(t)), 'k','LineWidth',2);
plot(t, confy(1)*ones(1,length(t)), 'k','LineWidth',2);
xlabel('kT (s)'); ylabel('chi squared test on \nu_k'); 
% 
figure; %subplot(211);
plot(t, cons1x, 'b','LineWidth',2); hold on;
plot(t, confx(2)*ones(1,length(t)), 'k','LineWidth',2);
plot(t, confx(1)*ones(1,length(t)), 'k','LineWidth',2);
xlabel('kT (s)'); ylabel('chi squared test on e_k'); 
% 
% Whiteness test:
figure; 
%subplot(211);
stem(delay, rho1(1,:), 'b'); hold on; %stem(delay, rho2(1,:), 'r');
plot(delay, -conf1*ones(1,length(delay)),'k:'); plot(delay, conf1*ones(1,length(delay)),'k:');
