function x = eq_dyn_pendulum(x0)

global dt;
global deltah;

options = odeset('MaxStep', deltah, 'RelTol', 1e-12);

[t, aux] = ode45('dyneq_pendulum', [0 dt], x0, options);
x = reshape(aux(end,:), size(x0,1), size(x0,2));



