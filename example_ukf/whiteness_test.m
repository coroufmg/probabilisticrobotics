function [rho_bar, delay, conf] = whiteness_test(x, j_max, perc)
%% function [rho_bar, delay, conf] = whiteness_test(x, j_max, pert)
% Single-run consistency indices:
% Time-average auto-correlation: rho_bar

N = size(x,2); % time span
n = size(x,1); % vector dimension

% If x is white, then rho_bar(l,j) is inside the acceptance region [-r, r],
% where r = r1/sqrt(N). For probability of 95%, then r1 = 1.96.
k0 = j_max + 1;
kf = N - j_max;
delay = [-j_max:1:j_max];
for l = 1:n % For each component
    for j = delay % For each delay
        rho_bar(l,j+j_max+1) = sum(x(l,k0:kf).*x(l,k0+j:kf+j))/sqrt(sum(x(l,k0:kf).^2)*sum(x(l,k0+j:kf+j).^2));
    end
end

conf = icdf('norm',1-(1-perc)/2,0, 1)/sqrt(kf-k0+1);