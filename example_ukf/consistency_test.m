function [epslon_k, conf] = consistency_test(x, Pxx, perc, tol)
%% function [epslon_k, conf] = consistency_test(x, Pxx, perc, tol)
% Single-run consistency indices:
% NIS or NEES: epslon_k, epslon_bar

N = size(x,2); % time span
n = size(x,1); % vector dimension

% Plot epslon_k as a function of time. If epslon_k remains inside the
% acceptance region calculated from the chi-squared test with n degrees of freedom, then the
% estimates are consistente with a given probability.

for k = 1:N
    %epslon_k(1,k) = (x(:,k)'*inv(Pxx(:,:,k) + tol*eye(size(x,1)))*x(:,k)); 
    epslon_k(1,k) = (x(:,k)'*inv(Pxx + tol*eye(size(x,1)))*x(:,k));
end

conf = [icdf('chi2',(1-perc)/2,n)  icdf('chi2',(1-(1-perc)/2),n)];

% If epslon_bar is inside the acceptance region calculated from the chi-squared test with n*N 
% degrees of freedom, then the estimates are consistente with a given probability.
% epslon_bar = mean(epslon_k);

