clear all
close all
T=0.5;
m=1;
A=[1 T; 0 1 ];
B=[0; T/m];
C=[1 0;0 1];
V=[0.2 0.05; 0.05 0.1];
W=[50 0; 0 0.2];
u=0;
X=[2;4]
P=[1 0; 0 1];

desenha_elipse(9*P, X);
hold on
plot(X(1), X(2), 'o')
axis equal

pause

for i=1:10
    % Etapa de predi��o
    X=A*X+B*u;
    P=A*P*A'+V;
    
    desenha_elipse(9*P, X);
    plot(X(1), X(2), 'o')
    axis equal
    
    pause
    
    % Etapa de corre��o
    
    Y=[2+T*(i)*4+sqrt(0.5)*randn(1); 4+sqrt(0.2)*randn(1)];
    
    
    S=C*P*C'+W;
    K=P*C'*inv(S);
    
    X=X+K*(Y-C*X);
    P=P-K*C*P;
    
    desenha_elipse(9*P, X);
    plot(X(1), X(2), 'o')
    axis equal
    
end